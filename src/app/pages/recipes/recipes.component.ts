import { Component, OnInit } from '@angular/core';
import { Recipe } from './recipe.model';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.scss'],
})
export class RecipesComponent implements OnInit {
  public selectedRecipe: Recipe = null;

  constructor() {}

  ngOnInit(): void {}

  onRecipeSelected = (recipe: Recipe) => {
    this.selectedRecipe = recipe;
  };
}
